<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Nette\Utils\ArrayList as NetteArrayList;

/**
 * @template ArrayListValue
 * @extends NetteArrayList<ArrayListValue>
 */
class ArrayList extends NetteArrayList
{
}

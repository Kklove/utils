<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Nette\Utils\Validators as NetteValidators;

class Validators extends NetteValidators
{
    /**
     * Throws exception if a variable is of unexpected type
     *
     * @param mixed $value
     * @param string $expected Expected types separated by pipe
     * @param string $label [OPTIONAL]
     * @return bool
     */
    public static function check($value, $expected, $label = 'variable'): bool
    {
        self::assert($value, $expected, $label);
        return true;
    }

    /**
     * Throws exception if an array field is missing or of unexpected type
     *
     * @param mixed[] $array
     * @param string $field Item
     * @param string|null $expected [OPTIONAL] Expected types separated by pipe
     * @param string $label [OPTIONAL]
     * @return bool
     */
    public static function checkField($array, $field, $expected = null, $label = 'item "%" in array'): bool
    {
        self::assertField($array, $field, $expected, $label);
        return true;
    }
}

<?php

declare(strict_types=1);

namespace Interitty\Utils;

use Interitty\PhpUnit\BaseTestCase;

/**
 * @coversDefaultClass Interitty\Utils\Validators
 */
class ValidatorsTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">

    /**
     * Tester of check
     *
     * @return void
     * @covers ::check
     * @group integration
     */
    public function testCheck(): void
    {
        $test = 'string';
        static::assertTrue(Validators::check($test, 'string'));
    }

    /**
     * Tester of checkField
     *
     * @return void
     * @covers ::checkField
     * @group integration
     */
    public function testCheckField(): void
    {
        $test = [
            'field' => 'string',
        ];
        static::assertTrue(Validators::checkField($test, 'field', 'string'));
    }
    // </editor-fold>
}
